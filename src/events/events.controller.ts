import { Crud, CrudController, Override } from '@nestjsx/crud';
import { Event } from './event.entity';
import { Body, Controller, UseGuards } from '@nestjs/common';
import { CreateEventDto, UpdateEventDto } from './events.dto';
import { EventsService } from './events.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { User } from '../users/user.entity';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { EventsOwnerGuard } from '../common/guards/events-owner.guard';

@Crud({
  model: {
    type: Event,
  },
  dto: {
    create: CreateEventDto,
    update: UpdateEventDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(EventsOwnerGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(EventsOwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(EventsOwnerGuard)],
    },
  },
})
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('events')
@ApiTags('Events')
export class EventsController implements CrudController<Event> {
  constructor(public service: EventsService) {}

  @Override()
  createOne(@Body() createEventDto: CreateEventDto, @CurrentUser() user: User) {
    return this.service.create(createEventDto, user);
  }
}
