import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Event } from './event.entity';
import { User } from '../users/user.entity';
import { DeepPartial } from 'typeorm';

@Injectable()
export class EventsService extends TypeOrmCrudService<Event> {
  constructor(@InjectRepository(Event) repo) {
    super(repo);
  }

  create(entity: DeepPartial<Event>, user: User) {
    return this.repo.save({ ...entity, userId: user.id });
  }
}
