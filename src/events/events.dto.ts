import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class CreateEventDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  name: string;
}

export class UpdateEventDto {
  @ApiProperty()
  @IsString()
  name?: string;
}
