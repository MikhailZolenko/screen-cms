import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../users/user.entity';
import { Screen } from '../screens/screen.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  userId: User['id'];

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @OneToMany(() => Screen, (screen) => screen.event, { onDelete: 'CASCADE' })
  screens: Screen[];

  @ManyToOne(() => User, (user) => user.events)
  user: User;
}
