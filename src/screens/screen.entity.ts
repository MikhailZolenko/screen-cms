import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Event } from '../events/event.entity';
import { Playlist } from '../playlists/playlist.entity';

@Entity()
export class Screen {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  eventId: Event['id'];

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @OneToOne(() => Playlist)
  @JoinColumn()
  playlist: Playlist;

  @ManyToOne(() => Event, (event) => event.screens, { onDelete: 'CASCADE' })
  event: Event;
}
