import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScreensService } from './screens.service';
import { ScreensController } from './screens.controller';
import { Screen } from './screen.entity';
import { EventsModule } from '../events/events.module';

@Module({
  imports: [TypeOrmModule.forFeature([Screen]), EventsModule],
  providers: [ScreensService],
  exports: [ScreensService],
  controllers: [ScreensController],
})
export class ScreensModule {}
