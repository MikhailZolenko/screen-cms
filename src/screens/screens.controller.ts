import { Crud, CrudController } from '@nestjsx/crud';
import { CreateScreenDto, UpdateScreenDto } from './screens.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Controller, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { Screen } from './screen.entity';
import { ScreensService } from './screens.service';
import { ScreensOwnerGuard } from '../common/guards/screens-owner.guard';

@Crud({
  model: {
    type: Screen,
  },
  dto: {
    create: CreateScreenDto,
    update: UpdateScreenDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(ScreensOwnerGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(ScreensOwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(ScreensOwnerGuard)],
    },
  },
})
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('screens')
@ApiTags('Screens')
export class ScreensController implements CrudController<Screen> {
  constructor(public service: ScreensService) {}
}
