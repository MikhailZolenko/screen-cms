import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Screen } from './screen.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ScreensService extends TypeOrmCrudService<Screen> {
  constructor(@InjectRepository(Screen) repo) {
    super(repo);
  }
}
