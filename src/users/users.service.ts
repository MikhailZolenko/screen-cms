import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { DeepPartial } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService extends TypeOrmCrudService<User> {
  constructor(@InjectRepository(User) repo) {
    super(repo);
  }

  async create(entity: DeepPartial<User>) {
    return this.repo.save({
      ...entity,
      password: await bcrypt.hash(entity.password, 10),
    });
  }

  findByEmail(email: string): Promise<User> {
    return this.repo.findOne({ email: email });
  }
}
