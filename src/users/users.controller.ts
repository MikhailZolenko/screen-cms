import { Crud, CrudController } from '@nestjsx/crud';
import { User } from './user.entity';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto, UpdateUserDto } from './users.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { Public } from '../common/decorators/public.decorator';
import { AccountOwnerGuard } from '../common/guards/account-owner.guard';

@Crud({
  model: {
    type: User,
  },
  dto: {
    create: CreateUserDto,
    update: UpdateUserDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(AccountOwnerGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(AccountOwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(AccountOwnerGuard)],
    },
  },
})
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('users')
@ApiTags('Users')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) {}

  @Post()
  @Public()
  create(@Body() createUserDto: CreateUserDto) {
    return this.service.create(createUserDto);
  }
}
