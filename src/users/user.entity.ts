import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Event } from '../events/event.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  password: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  email: string;

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @OneToMany(() => Event, (event) => event.user, { onDelete: 'CASCADE' })
  events: Event[];
}
