import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from 'src/common/decorators/current-user.decorator';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { CreateUserDto } from '../users/users.dto';
import { RegistrationStatus } from '../common/interfaces/registration-status.interface';
import { UserRegisterGuard } from '../common/guards/user-register.guard';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiTags('Auth')
  @ApiBody({ type: 'object', schema: { example: { email: '', password: '' } } })
  @ApiOperation({ summary: 'auth' })
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@CurrentUser() user) {
    return this.authService.login(user);
  }

  @ApiTags('Auth')
  @ApiBody({
    type: 'object',
    schema: { example: { name: '', email: '', password: '' } },
  })
  @ApiOperation({ summary: 'auth' })
  @UseGuards(UserRegisterGuard)
  @Post('auth/register')
  async register(
    @Body() createUserDto: CreateUserDto,
  ): Promise<RegistrationStatus> {
    const result: RegistrationStatus = await this.authService.register(
      createUserDto,
    );
    if (!result.success)
      throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
    return result;
  }
}
