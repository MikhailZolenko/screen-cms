import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { UsersService } from '../../users/users.service';

@Injectable()
export class UserRegisterGuard implements CanActivate {
  constructor(private usersService: UsersService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const userName = request.body.name;
    const userEmail = request.body.email;
    const userExists = await this.usersService.find({
      where: [{ name: userName }, { email: userEmail }],
    });

    if (userExists.length) {
      throw new HttpException(
        'email or username already exists',
        HttpStatus.UNAUTHORIZED,
      );
    }

    return !!userExists;
  }
}
