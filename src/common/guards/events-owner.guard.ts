import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { EventsService } from '../../events/events.service';

@Injectable()
export class EventsOwnerGuard implements CanActivate {
  constructor(private readonly eventsService: EventsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const eventId = request.params.id;
    const userId = request.user.id;
    const event = await this.eventsService.findOne({
      id: eventId,
      userId: userId,
    });

    return !!event;
  }
}
