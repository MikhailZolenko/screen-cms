import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { EventsService } from '../../events/events.service';
import { ScreensService } from '../../screens/screens.service';

@Injectable()
export class ScreensOwnerGuard implements CanActivate {
  constructor(
    private screensService: ScreensService,
    private eventsService: EventsService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const screenId = request.params.id;
    const screen = await this.screensService.findOne(screenId);
    const event = await this.eventsService.findOne({
      id: screen.eventId,
      userId: request.user.id,
    });

    return !!event;
  }
}
