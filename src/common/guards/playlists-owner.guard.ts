import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ScreensService } from '../../screens/screens.service';
import { PlaylistsService } from '../../playlists/playlists.service';

@Injectable()
export class PlaylistsOwnerGuard implements CanActivate {
  constructor(
    private screensService: ScreensService,
    private playlistsService: PlaylistsService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const playlistId = request.params.id;
    const playlist = await this.playlistsService.findOne({
      id: playlistId,
      userId: request.user.id,
    });

    return !!playlist;
  }
}
