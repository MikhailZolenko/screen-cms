import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ContentService } from '../../content/content.service';

@Injectable()
export class ContentOwnerGuard implements CanActivate {
  constructor(private contentService: ContentService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const contentId = request.params.id;
    const content = await this.contentService.findOne({
      id: contentId,
      userId: request.user.id,
    });

    return !!content;
  }
}
