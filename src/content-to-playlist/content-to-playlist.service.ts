import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ContentToPlaylist } from './content-to-playlist.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ContentToPlaylistService extends TypeOrmCrudService<ContentToPlaylist> {
  constructor(@InjectRepository(ContentToPlaylist) repo) {
    super(repo);
  }
}
