import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Content } from '../content/content.entity';
import { Playlist } from '../playlists/playlist.entity';

@Entity()
export class ContentToPlaylist {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  contentId: Content['id'];

  @Column({
    type: 'varchar',
  })
  playlistId: Playlist['id'];

  @Column({
    type: 'integer',
  })
  playbackDuration: number;

  @Column({
    type: 'integer',
  })
  order: number;

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @ManyToOne(() => Content)
  content: Content[];

  @ManyToOne(() => Playlist)
  playlist: Playlist[];
}
