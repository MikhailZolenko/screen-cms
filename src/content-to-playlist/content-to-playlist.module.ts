import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContentToPlaylist } from './content-to-playlist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ContentToPlaylist])],
})
export class ContentToPlaylistModule {}
