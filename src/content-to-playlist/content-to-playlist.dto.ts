import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class CreateContentToPlaylistDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  name: string;
}

export class UpdateContentToPlaylistDto {
  @ApiProperty()
  @IsString()
  name?: string;
}
