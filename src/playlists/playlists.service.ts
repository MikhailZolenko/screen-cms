import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Playlist } from './playlist.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial } from 'typeorm';
import { User } from '../users/user.entity';

@Injectable()
export class PlaylistsService extends TypeOrmCrudService<Playlist> {
  constructor(@InjectRepository(Playlist) repo) {
    super(repo);
  }

  create(entity: DeepPartial<Playlist>, user: User) {
    return this.repo.save({ ...entity, userId: user.id });
  }
}
