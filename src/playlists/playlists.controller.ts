import { Crud, CrudController, Override } from '@nestjsx/crud';
import { Playlist } from './playlist.entity';
import { PlaylistsService } from './playlists.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Body, Controller, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CreatePlaylistDto, UpdatePlaylistDto } from './playlists.dto';
import { PlaylistsOwnerGuard } from '../common/guards/playlists-owner.guard';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { User } from '../users/user.entity';

@Crud({
  model: {
    type: Playlist,
  },
  dto: {
    create: CreatePlaylistDto,
    update: UpdatePlaylistDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(PlaylistsOwnerGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(PlaylistsOwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(PlaylistsOwnerGuard)],
    },
  },
})
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('playlists')
@ApiTags('Playlists')
export class PlaylistsController implements CrudController<Playlist> {
  constructor(public service: PlaylistsService) {}

  @Override()
  createOne(
    @Body() createPlaylistDto: CreatePlaylistDto,
    @CurrentUser() user: User,
  ) {
    return this.service.create(createPlaylistDto, user);
  }
}
