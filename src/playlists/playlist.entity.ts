import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Screen } from '../screens/screen.entity';
import { ContentToPlaylist } from '../content-to-playlist/content-to-playlist.entity';
import { User } from '../users/user.entity';

@Entity()
export class Playlist {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  userId: User['id'];

  @Column({
    type: 'varchar',
  })
  screenId: Screen['id'];

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @OneToMany(
    () => ContentToPlaylist,
    (contentToPlaylist) => contentToPlaylist.playlist,
    { onDelete: 'CASCADE' },
  )
  contentToPlaylist: ContentToPlaylist[];
}
