import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Playlist } from './playlist.entity';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { ScreensModule } from '../screens/screens.module';
import { EventsModule } from '../events/events.module';

@Module({
  imports: [TypeOrmModule.forFeature([Playlist]), ScreensModule, EventsModule],
  providers: [PlaylistsService],
  exports: [PlaylistsService],
  controllers: [PlaylistsController],
})
export class PlaylistsModule {}
