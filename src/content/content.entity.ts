import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Playlist } from '../playlists/playlist.entity';
import { ContentToPlaylist } from '../content-to-playlist/content-to-playlist.entity';
import { User } from '../users/user.entity';

@Entity()
export class Content {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  userId: User['id'];

  @Column({
    length: 255,
    type: 'varchar',
  })
  mediaLink: string;

  @CreateDateColumn()
  createDate: string;

  @UpdateDateColumn()
  updateDate: string;

  @ManyToMany(() => Playlist)
  playlists: Playlist[];

  @OneToMany(
    () => ContentToPlaylist,
    (contentToPlaylist) => contentToPlaylist.content,
    { onDelete: 'CASCADE' },
  )
  contentToPlaylist: ContentToPlaylist[];
}
