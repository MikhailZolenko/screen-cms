import { Crud, CrudController, Override } from '@nestjsx/crud';
import { Content } from './content.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Body, Controller, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CreateContentDto, UpdateContentDto } from './content.dto';
import { ContentService } from './content.service';
import { ContentOwnerGuard } from '../common/guards/content-owner.guard';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { User } from '../users/user.entity';

@Crud({
  model: {
    type: Content,
  },
  dto: {
    create: CreateContentDto,
    update: UpdateContentDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(ContentOwnerGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(ContentOwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(ContentOwnerGuard)],
    },
  },
})
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('content')
@ApiTags('Content')
export class ContentController implements CrudController<Content> {
  constructor(public service: ContentService) {}

  @Override()
  createOne(
    @Body() createContentDto: CreateContentDto,
    @CurrentUser() user: User,
  ) {
    return this.service.create(createContentDto, user);
  }
}
