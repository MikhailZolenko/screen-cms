import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Content } from './content.entity';
import { DeepPartial } from 'typeorm';
import { User } from '../users/user.entity';

@Injectable()
export class ContentService extends TypeOrmCrudService<Content> {
  constructor(@InjectRepository(Content) repo) {
    super(repo);
  }

  create(entity: DeepPartial<Content>, user: User) {
    return this.repo.save({ ...entity, userId: user.id });
  }
}
