import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNumber, IsString } from 'class-validator';

export class CreateContentDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  name: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  mediaLink: string;

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  playbackDuration: number;
}

export class UpdateContentDto {
  @ApiProperty()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsString()
  mediaLink?: string;

  @ApiProperty()
  @IsNumber()
  playbackDuration: number;
}
