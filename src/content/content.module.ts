import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContentService } from './content.service';
import { ContentController } from './content.controller';
import { Content } from './content.entity';
import { ScreensModule } from '../screens/screens.module';
import { EventsModule } from '../events/events.module';
import { PlaylistsModule } from '../playlists/playlists.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Content]),
    ScreensModule,
    EventsModule,
    PlaylistsModule,
  ],
  providers: [ContentService],
  exports: [ContentService],
  controllers: [ContentController],
})
export class ContentModule {}
